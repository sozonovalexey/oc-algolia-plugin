<?php

return [
    'plugin' => [
        'name' => 'Algolia Search',
        'description' => 'Algolia Search integration for OctoberCMS.'
    ],
    'settings' => [
        'search' => 'Search',
        'search_description' => 'Outputs the search form.',
        'application_id' => 'Application ID',
        'application_id_description' => 'Your unique application identifier. It is used to identify yourself to API.',
        'api_key' => 'API Key',
        'api_key_description' => 'Your Search-Only API Key.',
        'index' => 'Index name',
        'index_description' => 'Your Index name.'
    ]
];