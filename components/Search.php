<?php namespace Xeor\Algolia\Components;


use Cms\Classes\ComponentBase;

class Search extends ComponentBase
{

    public $applicationID;

    public $apiKey;

    public $index;

    public function componentDetails()
    {
        return [
            'name' => 'xeor.algolia::lang.settings.search',
            'description' => 'xeor.algolia::lang.settings.search_description'
        ];
    }

    public function defineProperties()
    {
        return [
            'applicationID' => [
                'title' => 'xeor.algolia::lang.settings.application_id',
                'description' => 'xeor.algolia::lang.settings.application_id_description',
                'default' => 'ApplicationID',
                'type' => 'string',
                'required' => true,
            ],
            'apiKey' => [
                'title' => 'xeor.algolia::lang.settings.api_key',
                'description' => 'xeor.algolia::lang.settings.api_key_description',
                'default' => 'Search-Only-API-Key',
                'type' => 'string',
                'required' => true,
            ],
            'index' => [
                'title' => 'xeor.algolia::lang.settings.index',
                'description' => 'xeor.algolia::lang.settings.index_description',
                'default' => 'blog',
                'type' => 'string',
                'required' => true,
            ],
        ];
    }

    /**
     * Executed when this component is bound to a page or layout.
     */
    public function onRun()
    {

        $this->addJs('assets/vendor/algoliasearch/dist/algoliasearch.min.js');

        $this->prepareVars();

    }

    protected function prepareVars()
    {
        $this->applicationID = $this->page['applicationID'] = $this->property('applicationID');
        $this->apiKey = $this->page['apiKey'] = $this->property('apiKey');
        $this->index = $this->page['index'] = $this->property('index');
    }

}
