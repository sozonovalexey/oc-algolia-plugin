<?php namespace Xeor\Algolia;

use App;
use Event;
use BackendAuth;
use System\Classes\PluginBase;
use Illuminate\Foundation\AliasLoader;

/**
 * Algolia Plugin Information File
 */
class Plugin extends PluginBase
{
    
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'xeor.algolia::lang.plugin.name',
            'description' => 'xeor.algolia::lang.plugin.description',
            'author'      => 'Sozonov Alexey',
            'icon'        => 'icon-search'
        ];
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerConsole();
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        // Service provider
        App::register('\AlgoliaSearch\Laravel\AlgoliaServiceProvider');

        // Register alias
        $alias = AliasLoader::getInstance();
        $alias->alias('AlgoliaEloquentTrait', '\AlgoliaSearch\Laravel\AlgoliaEloquentTrait');
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Xeor\Algolia\Components\Search' => 'search',
        ];
    }

    /**
     * Register command line specifics
     */
    protected function registerConsole()
    {
        /*
         * Register console commands
         */
        $this->registerConsoleCommand('algolia.clear', 'Xeor\Algolia\Console\ClearIndices');
        $this->registerConsoleCommand('algolia.reindex', 'Xeor\Algolia\Console\Reindex');
    }
}
