<?php namespace Xeor\Algolia\Console;

use Exception;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class Reindex extends Command
{
    /**
     * The console command name.
     * @var string
     */
    protected $name = 'algolia:reindex';

    /**
     * The console command description.
     * @var string
     */
    protected $description = 'Reindex all your records.';

    /**
     * Create a new command instance.
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $pluginCode = $this->argument('plugin');
        $parts = explode('.', $pluginCode);
        $model = $this->argument('model');
        $safely = (string) $this->option('safely');
        $safely = $safely === 'true' ? true : false;

        if (count($parts) == 2 && $model) {
            $plugin = array_pop($parts);
            $author = array_pop($parts);
        }
        else {
            return $this->error('Invalid plugin or model name');
        }

        try {
            $this->info('Reindexing...');
            $class = '\\' .  $author . '\\' . $plugin . '\\Models\\' . $model;
            $obj = new $class();
            $obj::reindex($safely);
            $this->info('Finished!');
        }
        catch (Exception $ex) {
            $this->error($ex->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['plugin', InputArgument::REQUIRED, 'The name of the plugin. Eg: RainLab.Blog'],
            ['model', InputArgument::REQUIRED, 'The name of the model. Eg: Post'],
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['safely', null, InputOption::VALUE_REQUIRED, 'Safely reindex all your records.', true],
        ];
    }
}