# Algolia Search Plugin

This plugin integrates the Algolia Search API to the OctoberCMS.

## Documentation

### Quick Start

The following code adds search capabilities to your `Post` model creating a `Post` index:

```php
...
use AlgoliaSearch\Laravel\AlgoliaEloquentTrait;

class Post extends Model
{
    use AlgoliaEloquentTrait;
    ...
}
```

### Components

Name | Page variable | Description 
------------- | ------------- | -------------
Search | `{% component 'search' %}` | Outputs the search form

### Search Component

**Properties**

Property | Description | Example Value | Default Value
------------- | ------------- | ------------- | -------------
applicationID  | This is your unique application identifier. It is used to identify yourself to our API | P6NQJRS491 | -
apiKey  | This is the public API key to use in your frontend code. This key is only usable for search queries | 63a7291636e66f17399befdd79ac8abc | -
index  | Index name | articles | blog

**Variables available in templates**

* `{{ applicationID }}`
* `{{ apiKey }}`
* `{{ index }}`

**Example:**
~~~
<input class="autocomplete" type="text" placeholder="Start typing" id="autocomplete-algolia" spellcheck="false"/>
<script src="https://cdn.jsdelivr.net/autocomplete.js/0/autocomplete.jquery.min.js"></script>
<script type="text/javascript">
    (function($){
        $(document).ready(function() {
            var client = algoliasearch('{{ applicationID }}', '{{ apiKey }}');
            var index = client.initIndex('{{ index }}');
            $('#autocomplete-algolia').autocomplete(null, {
                source: $.fn.autocomplete.sources.hits(index),
                displayKey: 'Title'
            });
        });
    })(jQuery);
</script>
~~~

Learn more about [Algolia Search API Client for Laravel](https://github.com/algolia/algoliasearch-laravel)
and [Algolia Search API Client for JavaScript](https://github.com/algolia/algoliasearch-client-javascript). 

### Console Commands

    $ php artisan algolia:clear Rainlab.Blog Post
    $ php artisan algolia:reindex Rainlab.Blog Post --safely=true